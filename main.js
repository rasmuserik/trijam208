// must be power of 2
let width = 84;
let height = 48;
let frame = 0;
let dirs = [width + 1, width - 1, 1 - width, -1 - width];
function randomDir() {
  return dirs[Math.random() * 4 | 0];
}
let map = new Array(width * height);
let time = 999;
let vacuum = 99;
let level = 1;
let dead = 0;


function startscreen() {
  let startimage = `
####################################################################################
#   #   # ### #   ######################################## ###   # # #   # #########
## ### ##  #  # ########################################## ### ### # # ### #########
## ### ## # # #  ######################################### ###  ## # #  ## #########
## ### ## ### # ########################################## ### ### # # ### #########
## ##   # ### #   ########################################   #   ## ##   #   #######
####################################################################################
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                        ###                                       #
#                                        ###                                       #
#                                        ###                                       #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
#                                                                                  #
####################################################################################
########### # ## ###  # # # # # ### #####   #   # # ###### ##   ## ## # ############
########### # # # # ### # # # #  #  ## ############ ##### ### ### # ### ############
########### # #   # ### # # # # # # ############## ##### ####  ## # ## #############
########### # # # # ### # # # # ### ## ########## ##### ####### # # # ##############
############ ## # ##  ##  ##  # ### ############# # ## ######  ### ## # ############
####################################################################################

`.replace(/\n/g, "");
  let i = 0;
  let symbs = { "#": GROUND, " ": VOID, ".": PLAYER };
  for (let i = 0; i < map.length; ++i) {
    map[i] = GROUND;
  }
  for (const c of startimage) {
    if (typeof symbs[c] === "number") {
      map[i++] = symbs[c];
    }
  }
}

const VOID = 0;
const GROUND = 1;
const ENEMY = 2;
const PLAYER = 4;
let ctx;

let dir = 0;
let pos = 0;
let center = height / 2 * width + width / 2;
let enemypos = [];
let enemydir = [];

initUI();
reset();

function randint(n) {
  return Math.random() * n | 0
}

function reset() {
  dead = 0;
  time = 999;
  vacuum = 99;
  enemypos = [];
  enemydir = [];
  pos = width*height / 2 + width / 2;
  dir = 0;
  for (let i = 0; i < Math.pow(2, ((level)/ 2|0)) ; ++i) {
    enemypos.push(12 * width + 2 + randint(80) + randint(30) * width );
    enemydir.push(randomDir());
  }
  for (let i = 0; i < width * height; ++i) {
    map[i] = VOID;
  }
  for (let x = 0; x < width; ++x) {
    set(x, 0, GROUND);
    set(x, height - 1, GROUND);
    set(x, height - 2, GROUND);
    set(x, height - 3, GROUND);
    set(x, height - 4, GROUND);
    set(x, height - 5, GROUND);
    set(x, height - 6, GROUND);
  }
  for (let y = 0; y < height; ++y) {
    set(0, y, GROUND);
    set(width - 1, y, GROUND);
  }
startscreen();
  fill();
}
function set(x, y, val) {
  let p = x + y * width;
  map[p] = val;
}

function putnum(pos, n) {
  let nums = `
... .#. .#. ##. ##. #.. ### .## ### ### .## 
... #.# ##. ..# ..# #.# #.. #.. ..# #.# #.# 
... #.# .#. .#. .#. ### ##. ##. ..# ### .##
... #.# .#. #.. ..# ..# ..# #.# .#. #.# ..# 
... .#. ### ### ##. ..# ##. .#. .#. ### ..#`.replace(/[^.#]/g, '');

  for(let x = 0; x < 3; ++x) {
  for(let y = 0; y < 5; ++y) {
    let c = nums[(n+1)*3 + x + 33*y];
    map[pos + x + y*width]= c === '.'?GROUND :VOID;
  }
  }
}


function blit() {
  for (let x = 0; x < width; ++x) {
    for (let y = 0; y < height; ++y) {
      ctx.fillStyle = ({
          [GROUND]: true,
          [VOID]: false,
          [ENEMY]: true,
          [PLAYER]: (frame & 1) === 0,
        })[map[x + y * width]]
        ? "#c7f0d8"
        : "#43523d";
      ctx.fillRect(x * 8, y * 8, 8, 8);
    }
  }
}
function initUI() {
  let root = document.querySelector("#app");
  window.x = root;
  let viewwidth = Math.min(root.clientWidth - 16, (root.clientHeight - 16) * 2);
  root.innerHTML = `
    <style>
      #viewport {
        position: absolute;
        top: 8px;
        left: 8px;
        width: ${viewwidth}px;
        height: ${viewwidth / 2}px;
        border: 0.2px solid white;
        box-shadow: 2px 2px 6px rgba(0,0,0,0.5);
        image-rendering: crisp-edges;
      }
      .bg {
        position: fixed;
        top: 0; left:0; right:0; bottom: 0;
        background: #333;
        overflow: hidden;
      }
      .fab {
        font-family: sans-serif;
        position: absolute;
        width: 64px;
        height: 64px;
        background: rgba(255,255,255,.2);
        border-radius: 32px;
        display: flex;
        font-weight: bold;
        justify-content: center;
        align-items: center;
        border: 0.2px solid white;
        box-shadow: 2px 2px 6px rgba(0,0,0,0.5);
        font-size: 24px;
      }
      .fab.w {
        right: 55px;
        bottom: 105px;
      }
      .fab.s {
        right: 55px;
        bottom: 5px;
      }
      .fab.d {
        right: 5px;
        bottom: 55px;
      }
      .fab.a {
        right: 105px;
        bottom: 55px;
}

  </style>
  <div class="bg" 
  ontouchstart="e=>e.preventDefault()"
  ontouchmove="e=>e.preventDefault()"
></div>
  <canvas id="viewport"></canvas>
  <div class="fab w" onpointerdown="handleKey('w')" onpointermove="handleKey('w')" >W</div>
  <div class="fab a" onpointerdown="handleKey('a')" onpointermove="handleKey('a')" >A</div>
    <div class="fab s" onpointerdown="handleKey('s')" onpointermove="handleKey('s')" >S</div>
  <div class="fab d" onpointerdown="handleKey('d')" onpointermove="handleKey('d')" >D</div>
  `;
  let canvas = document.querySelector("#viewport");
  canvas.width = width * 8;
  canvas.height = height * 8;
  ctx = canvas.getContext("2d");
}
window.onkeydown = ({ key }) => handleKey(key);

window.handleKey = function handleKey(key) {
  key =
    ({ ArrowLeft: "a", ArrowUp: "w", ArrowRight: "d", ArrowDown: "s" }[key] ||
      key).toLowerCase();
  if ("wasd".includes(key)) {
    let newdir = {
      w: -width,
      s: width,
      a: -1,
      d: 1,
    }[key];
    if (newdir !== -dir) {
      dir = newdir;
    }
  }
};

function inc(pos, dir) {
  if (pos + dir < 0) dir = 0;
  if (pos + dir >= width * height) dir = 0;
  if (
    Math.abs(dir) < 2 && ((pos + dir) / width | 0) !==
      (pos / width | 0)
  ) dir = 0;
  return pos + dir;
}
let wasGround = true;
function fill() {
  let newmap = [];
  for (let i = 0; i < width * height; ++i) {
    newmap[i] = GROUND;
  }
  let voids = [...enemypos];
  let i;
  for (i = 0; i < voids.length; ++i) {
    let pos0 = voids[i];
    for (let d of [1, -1, width, -width]) {
      let p = pos0 + d;
      if (
        p >= 0 && p <= width * height && map[p] === VOID && newmap[p] === GROUND
      ) {
        newmap[p] = VOID;
        voids.push(p);
      }
    }
  }
  for(let i = 0; i < width * 6; ++i) {
    newmap[i] = map[i];
    newmap[width* height - i] = map[width*height - i];
  }
  map = newmap;

  let taken = (100 - 100 * i / 2820) | 0;
  vacuum = 100 - taken | 0;
  if (taken >= 50) {
    level++;
    reset();
  }
}
function die() {
  dir = 0;
  dead = 1;
}
function mainloop() {
  ++frame;
  if(dead) {
    blit();
    if(dir) {
      level = 1;
      reset();
    }
    return
  }


  let fast = (level&1 == 1) 
  for (let n = 0; n < (fast?2:1); ++n) {
    for (let i = 0; i < enemypos.length; ++i) {
      map[enemypos[i]] = VOID;
      let p = enemypos[i] + enemydir[i];
      for (let j = 0; j < 1000 && map[p] !== VOID; ++j) {
        enemydir[i] = randomDir();
        if (map[p] === PLAYER) die();
        p = enemypos[i] + enemydir[i];
      }
      enemypos[i] = p;
      map[enemypos[i]] = ENEMY;
    }
  }
  if (fast || (frame % 2.5 < 1)) {
    if (dir) {
      if (wasGround) {
        set(pos, 0, GROUND);
      }
      pos = inc(pos, dir);
      let isGround = map[pos] === GROUND;
      if (!wasGround && map[pos] === PLAYER) {
        //die();
      }
      if (isGround && !wasGround) {
        fill();
        dir = 0;
      }
      wasGround = isGround;
    }
    set(pos, 0, PLAYER);
  } else {
    set(pos, 0, PLAYER);
  }

  putnum(80 + width, level); 

  time -= (level&1 === 1) ? 1.3 : .1;
  if(time < 0) {
    time = 0;
    die()
  }
  putnum(21 + width, (time / 100 % 10) |0);
  putnum(25 + width, (time / 10 % 10) |0);
  putnum(29 + width, (time % 10) |0);

  putnum(41 + width * 42, (vacuum / 10 % 10) |0);
  putnum(45 + width*42, (vacuum % 10) |0); 
  blit();
}
setInterval(mainloop, 1000 / 15);
